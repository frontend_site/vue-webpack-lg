// const { NodeSSH } = require('node-ssh');
const fs = require("fs-extra");

class UploadWebpackPlugin {
  constructor(options) {
    console.log(options);
  }

  apply(compiler) {
    // compiler.hooks.afterEmit
    // console.dir(compiler);

    compiler.hooks.emit.tapAsync("clean", (compilation, callback) => {
      if (fs.pathExistsSync(compiler.outputPath)) {
        fs.remove(compiler.outputPath)
          .then(() => {
            callback();
            console.log("===== cleaned =====");
          })
          .catch((err) => {
            console.error(err);
          });
      } else {
        callback();
      }
    });
  }
}

module.exports = UploadWebpackPlugin;
