module.exports = {
  plugins: [
    require("autoprefixer"),
    require("./plugins/plugin-media.js")({
      prefix: "pl-",
      rules: {
        phone: ["320px", "375px"],
        pad: ["375px", "640px"],
        small: ["640px", "960px"],
      },
    }),
  ],
};
