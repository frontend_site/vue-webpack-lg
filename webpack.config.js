const webpack = require("webpack");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { VueLoaderPlugin } = require("vue-loader");
const UploadWebpackPlugin = require("./utils/clean-plugin/index.js");

const config = {
  devtool: "inline-source-map",
  mode: "development",
  entry: "./src/main.js",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "[name]-[chunkhash:6].js",
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          { loader: "style-loader" },
          { loader: "css-loader" },
          { loader: "postcss-loader" },
        ],
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        use: [
          {
            loader: "./utils/file-loader/index.js",
            options: {
              modules: true,
              limit: 10240,
              outputPath: "./asset",
            },
          },
        ],
      },
      {
        test: /\.scss$/i,
        use: [
          "style-loader",
          "css-loader",
          {
            loader: "sass-loader",
            options: {
              implementation: require("dart-sass"),
              // additionalData: `@import "@/styles/index.scss";`,
            },
          },
          {
            loader: "sass-resources-loader",
            options: {
              resources: [
                // 引入全局 SasS 变量文件
                "src/styles/index.scss",
              ],
            },
          },
        ],
      },
      {
        test: /\.vue$/,
        use: [
          {
            loader: "vue-loader",
            options: {
              options: {
                loaders: {
                  // scss: "style-loader!css-loader!sass-loader",
                  scss: [
                    "style-loader",
                    "css-loader",
                    {
                      loader: "sass-loader",
                      options: {
                        implementation: require("dart-sass"),
                      },
                    },
                  ],
                },
              },
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: "index.html",
      template: path.resolve(__dirname, "public", "index.html"),
    }),
    new UploadWebpackPlugin({
      host: "losa.fun",
      password: "password",
    }),
    new VueLoaderPlugin(),
    new webpack.ProvidePlugin({
      Vue: ["vue"],
      ref: ["vue", "ref"],
      reactive: ["vue", "reactive"],
      faker: ["faker"],
      watch: ["vue", "watch"],
      onMounted: ["vue", "onMounted"],
      nextTick: ["vue", "nextTick"],
      computed: ["vue", "computed"],
      watchEffect: ["vue", "watchEffect"],
    }),
    // new webpack.HotModuleReplacementPlugin(),
  ],
  resolve: {
    extensions: [".js", ".vue", ".scss"],
    alias: {
      "@": path.resolve(__dirname, "src"),
      vue$: "vue/dist/vue.esm-browser.js",
    },
  },
  devServer: {
    port: 8080,
    hot: false,
    open: true,
    historyApiFallback: true,
    proxy: {
      "/api": {
        target: "http://localhost:8000",
        changeOrigin: true,
        pathRewrite: {
          "^/api": "/",
        },
      },
    },
    // setupMiddlewares(devServer) {},
  },
};

module.exports = config;
