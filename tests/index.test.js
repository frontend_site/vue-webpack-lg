const { unit } = require('../src/utils/unit.js');

function sum(a, b) {
  return a + b;
}

test("run", () => {
  expect(sum(1, 2)).toBe(3);
});

test("unit", () => {
  const code = "This is a Example"
  expect(unit(code)).toBe(code);
});
