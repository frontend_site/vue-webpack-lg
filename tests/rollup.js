// let n = {
//   c: 0,
// };

// function increase() {
//   ++n.c;
// }

// module.exports = {
//   val: n,
//   increase,
// };

// console.log(__dirname, __filename);

const vm = require("vm");

const hl = {};

// 创建一个沙盒上下文
const sandbox = vm.createContext({
  scheduler: function (name, exports) {
    exports = {
      hl: 2090,
    };
    // console.log(exports);
    console.log("Hello, " + name + "!");
  },
  module: {
    exports: {},
  },
});

// 在沙盒上下文中执行函数，并传递参数
vm.runInContext(
  `
(() => {
  const h = this.module;
  this.scheduler.apply(h.exports, [90, h.exports])
})()
`,
  sandbox
);

console.log(sandbox.module);
