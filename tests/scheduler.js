// const { val, increase } = require("./rollup");
// console.log(increase, val);
// increase()
// console.log(increase, val);

// 在 CommonJS 和 ES 模块中，都是按值的引用来导入和导出模块的
const tVal = require('./es-commom.js');
const dVal = require('./es-commom.js');

tVal.age = 32;
// 按值的引用 验证代码
console.log(tVal);
console.log(dVal);
console.log(tVal === dVal);

function $require(id) {
  const vm = require("vm");
  const fs = require("fs");
  const path = require("path");
  console.log(path.resolve(id));
  $require.cache ??= Object.create(null);

  const $filename = "";
  const $dirname = "";

  const $module = {
    filename: $filename,
    id: ".",
    exports: {},
    children: [],
  };

  const exports = $module.exports;

  function add(a, b) {
    return a + b;
    this.ln = 29;
  }

  // const script = new vm.Script(`add(4, 9)`);
  const sandbox = {
    m: 1,
    n: 2,
    add(a, b) {
      this.ln = a + b;
    },
  };
  // script.runInNewContext(sandbox);
  vm.runInNewContext(`add(4, 9)`, sandbox);

  console.log(sandbox);

  $require.cache[$filename] = $module;
  return $module.exports;
}

$require("./shell");