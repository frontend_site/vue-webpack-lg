const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const config = {
  mode: 'development',
  entry: path.resolve(__dirname, "../src", "main.js"),
  output: {
    path: path.resolve(__dirname, "../dist"),
    filename: '[name]-[hash:6].js'
  },
  module: {

  },
  plugins: [
    new HtmlWebpackPlugin({
      filename:'index.html',
      template:'/public/index.html'
  })
  ],
  resolve: {
    extensions: [".js", ".vue", ".scss"]
  }
}

module.exports = config;