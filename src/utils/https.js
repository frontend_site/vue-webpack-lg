import axios from "axios";

const https = axios.create({
  baseURL: "/api",
  timeout: 12000,
});

https.interceptors.request.use(
  (config) => {
    // 在发送请求之前做些什么
    return config;
  },
  (error) => {
    // 对请求错误做些什么
    return Promise.reject(error);
  }
);

const htls = ["ERR_BAD_RESPONSE"];
// 添加响应拦截器
https.interceptors.response.use(
  (response) => {
    // 对响应数据做点什么
    return response.data;
  },
  (error) => {
    // 对响应错误做点什么
    if (htls.includes(error.code)) {
      return axios.post("/api/auth");
    }
    return Promise.reject(error);
  }
);

export default https;
