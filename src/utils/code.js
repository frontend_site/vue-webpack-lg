function debounce(fn, delay) {
  let timer = null;
  return (...args) => {
    if (timer) {
      clearTimeout(timer);
    }
    timer = setTimeout(() => {
      fn.apply(this, args);
    }, delay);
  };
}

function throttle(fn, timeout) {
  let timer = null;
  return (...args) => {
    if (timer) return;
    timer = setTimeout(() => {
      timer = null;
      fn.apply(this, args);
    }, timeout);
  };
}

function throttle(fn, limit) {
  let start = 0;
  return (...args) => {
    let now = Date.now();
    if (now - start >= limit) {
      start = now;
      fn.apply(this, args);
    }
  };
}
