function useSizeVirtual(cachedVal = new Map()) {
  const rangeAveSize = ref(0);

  const getEstimateSize = () => {
    return rangeAveSize.value;
  };

  const getIndexOffset = (cptedVal = 0) => {
    let offset = 0;
    let indexSize = 0;
    for (let index = 0; index < cptedVal; index++) {
      const key = indexToIdCache[index];
      indexSize = cachedVal.get(key);
      offset += isNaN(indexSize) ? getEstimateSize() : indexSize;
    }

    currentOffset.value = offset;

    return offset;
  };

  const getScrollOvers = (offset) => {
    if (offset <= 0) return 0;
    // console.log(cachedVal);

    let low = 0;
    let middle = 0;
    let middleOffset = 0;
    let high = all;

    while (low <= high) {
      middle = low + ~~((high - low) / 2);
      middleOffset = getIndexOffset(middle);
      // console.log('low', low, offset);
      // console.log("middle", middle, middleOffset);

      if (middleOffset === offset) {
        return middle;
      } else if (middleOffset < offset) {
        low = middle + 1;
      } else if (middleOffset > offset) {
        high = middle - 1;
      }
    }
    return low > 0 ? --low : 0;
  };

  return { rangeAveSize, getEstimateSize, getIndexOffset, getScrollOvers };
}
