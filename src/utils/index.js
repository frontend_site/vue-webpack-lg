import faker from "faker";
import logo from "@/assets/logo.png";

console.log(faker);

export function logger() {
  console.log(logo);
  console.log("This is a example");
}

function random() {
  return 10 + ~~(Math.random() * 80);
}

export function listCreate(nums = 10000) {
  const list = [];
  for (let i = 0; i < nums; i++) {
    list.push({
      email: faker.internet.email(),
      index: i,
      id: i + 1,
      name: faker.name.findName(),
      music: faker.music.genre(),
      photo: faker.internet.avatar(),
      words: faker.lorem.words(random()),
    });
  }

  return list;
}

export function debounce(func, delay = 500) {
  let timeout;

  return function (...args) {
    const context = this;

    clearTimeout(timeout);
    timeout = setTimeout(() => {
      func.apply(context, args);
    }, delay);
  };
}

export function throttle(func, delay) {
  let timeout = null;

  if (delay <= 0) return func;

  return function (...args) {
    const context = this;

    if (timeout) return;

    timeout = setTimeout(() => {
      timeout = null;
      func.apply(context, args);
    }, delay);
  };
}
