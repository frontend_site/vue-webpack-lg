import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/",
    component: () => import("../views/home/index.vue"),
  },
  {
    path: "/mobile-page",
    component: () => import("../views/users/index.vue"),
  },
  {
    path: "/csdn-virtual",
    component: () => import("../views/csdn/index.vue"),
  },
  {
    path: "/verify",
    component: () => import("../views/verify/index.vue"),
  },
  {
    path: "/compose",
    component: () => import("../views/compose/index.vue"),
  },
  {
    path: "/dynamic",
    component: () => import("../views/dynamic/index.vue"),
  },
  {
    path: "/stream",
    component: () => import("../views/stream/index.vue"),
  },
  {
    path: "/cached",
    component: () => import("../views/cached/index.js"),
  },
  {
    path: "/remote",
    component: () => import("../views/next-step/index.vue"),
  },
];

// 3. 创建路由实例并传递 `routes` 配置。
export default createRouter({
  // 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
  history: createWebHistory(),
  routes, // `routes: routes` 的缩写
});
