import https from "@/utils/https.js";

export const login = () => {
  return https.post("/login");
};

export const reflush = () => {
  return https.post("/reflush");
};

export const getUserInfo = () => {
  return https.get("/user");
};

export const verifyAuth = () => {
  return https.post("/auth-catch");
};
