import { h, cloneVNode, getCurrentInstance } from "vue";

export const normal = h({
  name: "normal",
  data() {
    return {
      count: 0,
    };
  },
  render() {
    const handleClick = () => {
      ++this.count;
    };

    return h("div", [
      h("div", "Hello Normal"),
      h(
        "div",
        {
          onClick: handleClick,
        },
        `Count Computed: ${this.count}`
      ),
    ]);
  },
  mounted() {
    console.log("mounted", this.count);
    console.log("===================");
  },
});

export const cached = h({
  name: "cached",
  data() {
    return {
      count: 0,
    };
  },
  render() {
    const handleClick = () => {
      ++this.count;
    };

    return h("div", [
      h("div", "Hello Cached"),
      h(
        "div",
        {
          onClick: handleClick,
        },
        `Count Computed: ${this.count}`
      ),
    ]);
  },
});

export const reflush = h({
  name: "reflush",
  data() {
    return {
      count: 0,
    };
  },
  render() {
    const handleClick = () => {
      ++this.count;
    };
    return h("div", [
      h("div", "Hello Reflush"),
      h(
        "div",
        {
          onClick: handleClick,
        },
        `Count Computed: ${this.count}`
      ),
    ]);
  },
});

const dicts = new Map([
  ["cached", cached],
  ["normal", normal],
]);

let current = '';
const caches = new Map();

export default {
  name: "ViewCached",
  __isKeepAlive: true,
  props: {
    includes: {
      type: Array,
      default: () => [],
    },
  },
  mounted() {
    this.cacheSubtree();
  },
  updated() {
    this.cacheSubtree();
  },
  methods: {
    cacheSubtree() {
      const dtl = getCurrentInstance();
      if (caches.has(current)) {
        console.log(dtl.subTree);
      } else {
        caches.set(current, dtl.subTree);
      }
    },
  },
  setup(props, context) {
    return () => {
      // console.log(context);
      const slots = context.slots.default();
      if (slots.length < 1) return h(null);

      const Vnode = slots[0];
      const name = Vnode.type.name;

      if (props.includes.includes(name)) {
        if (caches.get(name)) {
          const Cnode = caches.get(name);
          // console.log(Cnode);
          console.log(cloneVNode(Cnode));
          console.log("---------------");

          return Cnode.el ? cloneVNode(Cnode) : Cnode;
        } else {
          current = name;
          return Vnode;
        }
      } else {
        return slots;
      }
    };
  },
};
