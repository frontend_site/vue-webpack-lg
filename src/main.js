import { createApp } from "vue";
import "./style.css";
import "./styles/index.scss";
import router from "./router/index.js";
import App from "./App.vue";
import VirtualList from "./components/virtual-list.vue";
import CoreVirtual from "./components/core-virtual.vue";
import CsdnVirtual from "./components/csdn-virtual/index.vue";
import "@/mocks/index.js";
// require('@/mocks/index.js');
import * as utils from "@/utils/index.js";

// utils.logger();

createApp(App)
  .use(router)
  .component("virtual-list", VirtualList)
  .component("core-virtual", CoreVirtual)
  .component("csdn-virtual", CsdnVirtual)
  .mount("#app");
