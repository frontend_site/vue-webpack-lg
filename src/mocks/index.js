import Mock from "mockjs";

Mock.mock("/api/user", "get", () => {
  return {
    age: 29,
    email: Mock.mock("@email"),
    id: 1,
    sex: 1,
    username: Mock.mock("@name"),
  };
});

Mock.mock("/api/login", "post", {
  code: 200,
  msg: "登录成功",
  user: { name: Mock.mock("@name"), age: 18, rank: 10, email: "@email()" },
  token: "joaaohiefuopieeaskfoefeop", //token随便乱写
});

Mock.mock("/api/auth", "post", {
  code: 401,
  user: { name: Mock.mock("@name"), age: 18, rank: 10, email: "@email()" },
  token: "joaaohiefuopieeaskfoefeop", //token随便乱写
});

Mock.mock("/api/reflush", "post", {
  code: 200,
  user: { name: Mock.mock("@name"), age: 18, rank: 10, email: "@email()" },
  token: "joaaohiefuopieeaskfoefeop", //token随便乱写
});