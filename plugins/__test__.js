const postcss = require("postcss");
const plugin = require("./plugin-media.js");

const tmpl = `
  .app {
    font-size: 14px; /* this is a comment */
    color: #ffffff;
  }
  .router-list {
    color: dark;
    margin: 20px;
    background-color: dark;
  }
  .app .panel {
    width: 360px;
    background-color: dark;
  }
  @media screen and (max-width: 600px) {
    /* 当屏幕小于或等于 600px 时应用样式 */
    .app {
      background-color: lightblue;
    }
  }
`;

postcss([plugin, require("postcss-nested")])
  .process(tmpl, { from: "input.css", to: "output.css" })
  .then((result) => {
    console.log(result.css);
  });
