const Koa = require("koa");
const router = require("./routes");
const cors = require("./_cors");

const app = new Koa();

app.use(cors);
app.use(router.routes());

app.use(async (ctx) => {
  ctx.body = "Hello World";
});

app.listen(3000, () => {
  console.log("Server running on http://localhost:3000");
});
